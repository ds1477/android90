package com.example.android90;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.io.IOException;
import java.util.ArrayList;

public class SearchPhotos extends AppCompatActivity {

    AlbumManager AM;
    private ArrayList<Album> albums;
    private ArrayAdapter<Album> arrayAdapter;
    private ArrayList<String> tags;

    private AutoCompleteTextView tagValue1;
    private AutoCompleteTextView tagValue2;


    private RadioButton andOperator;
    private RadioButton orOperator;
    private RadioButton noSecondTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

        }
        try {
            AM = AlbumManager.deserialize();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        albums = AM.getAllAlbums();
        for (int i = 0; i < albums.size(); i++) {
            ArrayList<Photo> photos = albums.get(i).getPhotos();
            for (int j = 0; j < photos.size(); j++) {
                Photo photo = photos.get(j);
                tags = photo.getTags();
            }
        }

        tagValue1 = findViewById(R.id.tagValue1);
        tagValue2 = findViewById(R.id.tagValue2);

        tagValue1.setAdapter(new ArrayAdapter<String>(SearchPhotos.this, android.R.layout.simple_list_item_1, tags));
        tagValue2.setAdapter(new ArrayAdapter<String>(SearchPhotos.this, android.R.layout.simple_list_item_1, tags));

        orOperator = findViewById(R.id.or);
        andOperator = findViewById(R.id.and);
        noSecondTag = findViewById(R.id.none);
        tagValue1 = findViewById(R.id.tagValue1);
        tagValue2 = findViewById(R.id.tagValue2);

        if (noSecondTag.isChecked()) {
            tagValue2.setFocusable(false);
        } else {
            tagValue2.setFocusable(true);
        }

    }

    public void seeResults(View view) { //todo
        Album searchResults = new Album("Search Results");
        String letter1;
        String letter2;
        String fullTag1 = tagValue1.getText().toString().toLowerCase();
        System.out.println("Flag1: " + fullTag1);
        if (fullTag1.length() < 7) {
            Bundle bundle = new Bundle();
            bundle.putString(AlbumDialogFragment.MESSAGE_KEY,
                    "Search criteria was invalid. Be sure to enter valid tag values. For example, [location: new york]");
            DialogFragment newFragment = new AlbumDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        } else {
            letter1 = Character.toString(fullTag1.charAt(1)); //either "l" or "p"
        }



        String fullTag2 = tagValue2.getText().toString().toLowerCase();
        if (fullTag2.length() < 7) {
            letter2 = "z";
        } else {
            letter2 = Character.toString(fullTag2.charAt(1)); //either "l" or "p"
        }

        if (!andOperator.isChecked() && !orOperator.isChecked() && !noSecondTag.isChecked()) {
            Toast.makeText(getApplicationContext(), "Please select an operator.", Toast.LENGTH_SHORT).show();
            return;
        }

        //one tag is searched
        //noSecondTag is selected

        if ((letter1.equals("p") || letter1.equals("l")) && noSecondTag.isChecked()) {
                System.out.println("Entered: " + fullTag1);
                ArrayList<Photo> photos = AM.getAllPhotos();
                for (Photo photo : photos) {
                    ArrayList<String> tags = photo.getTags();
                    for (String tag : tags) {
                        System.out.println(tag);
                        tag = tag.toLowerCase();
                        if (tag.equals(fullTag1)) {
                            searchResults.addStockPhoto(photo);
                            break;
                        }
                    }
                }

        }

        //two tags are searched
        //or operator
        //and operator
        else if (((letter1.equals("p") || letter1.equals("l")) && (letter2.equals("p") || letter2.equals("l"))) && (orOperator.isChecked() || andOperator.isChecked())) {

            if (orOperator.isChecked()){
                    ArrayList<Photo> photos = AM.getAllPhotos();
                    for (Photo photo : photos) {
                        ArrayList<String> tags = photo.getTags();
                        for (String tag: tags) {
                            tag = tag.toLowerCase();
                            if (tag.equals(fullTag1) || tag.equals(fullTag2)) {
                                searchResults.addStockPhoto(photo);
                                break;
                            }
                        }
                    }

            } else {
                ArrayList<Photo> photos = AM.getAllPhotos();
                for (Photo photo : photos) {
                    ArrayList<String> tags = photo.getTags();
                    Boolean t1 = false;
                    Boolean t2 = false;
                    for (String tag: tags) {
                        tag = tag.toLowerCase();
                        if (tag.equals(fullTag1)) {
                            t1 = true;
                        }
                        if (tag.equals(fullTag2)) {
                            t2 = true;
                        }
                    }
                    if (t1 && t2) {
                        searchResults.addStockPhoto(photo);
                    }
                }
            }

        } else {
            Bundle bundle = new Bundle();
            bundle.putString(AlbumDialogFragment.MESSAGE_KEY,
                    "Search criteria was invalid. Be sure to enter valid tag values. For example, [location: new york]");
            DialogFragment newFragment = new AlbumDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }

        try {
            AM.setSearchResults(searchResults);
            AlbumManager.serialize(AM);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, SearchResults.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}

