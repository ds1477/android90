package com.example.android90;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class InsideAlbum extends AppCompatActivity {

    public static final String ALBUM_INDEX = "albumIndex";
    public static final String ALBUM_NAME = "albumName";
    public static final int RENAME_CODE = 1;
    public static final int RESULT_IMAGE = 2;
    public static final int REQUEST_CODE = 3;

    private int albumIndex;
    //private TextView albumName;
    private TextView album_name;

    private ArrayList<Photo> photos;
    private ListView listview;
    final Context c = this;
    //public static AlbumManager manager = new AlbumManager();

    private static ArrayList<Album> albums = new ArrayList<Album>();
    AlbumManager AM;
    Album activeAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opened_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //photos = new ArrayList<String>();
        //photos.add("For Testing");

        album_name = findViewById(R.id.album_name);


        try {
            AM = AlbumManager.deserialize();
            activeAlbum = AM.getActiveAlbum();
            photos = activeAlbum.getPhotos();
            album_name.setText(activeAlbum.name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        listview = findViewById(R.id.photos_list);
        listview.setAdapter(new ImageAdapter(this, R.layout.photos, photos));
        listview.setOnItemClickListener((p, V, pos, id) -> displayPhoto(pos));


    }

    @SuppressWarnings("deprecation")
    public void addPhoto(View view) {
        requestPermissions(new String[] { Manifest.permission.MANAGE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE },
                REQUEST_CODE);
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/");
            startActivityForResult(i, RESULT_IMAGE);

    }

    public void deletePhoto(View view) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, DeletePhoto.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            if (activeAlbum.hasPhoto(picturePath)) {
                Toast.makeText(getApplicationContext(), "Photo is already in album!", Toast.LENGTH_LONG).show();
                return;
            }
            Photo add = new Photo(picturePath);
            activeAlbum.addStockPhoto(add);
            try {
                AlbumManager.serialize(AM);
            } catch (IOException e) {
                e.printStackTrace();
            }
            photos = activeAlbum.getPhotos();
            listview.setAdapter(new ImageAdapter(this, R.layout.photos, photos));
            listview.setOnItemClickListener((p, V, pos, id) -> displayPhoto(pos));

//            ImageView imageView = (ImageView) findViewById(R.id.testPic);
//            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//            System.out.println("Ok");

        }


    }

    public boolean onOptionsItemSelected(MenuItem item){
        Bundle bundle = new Bundle();
        //bundle.putParcelable("AM", AM);
        Intent intent = new Intent(this, Albums.class);
        intent.putExtras(bundle);
        startActivity(intent, bundle);
        return true;
    }

    public void displayPhoto(int pos) {
        //AM.setActivePhoto(photos.get(pos).getPath());
        Bundle bundle = new Bundle();
        Photo photo = photos.get(pos);
        AM.setActivePhoto(photo.getPath());
        try {
            AlbumManager.serialize(AM);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //bundle.putParcelable("AM", AM);
        bundle.putInt("index", pos);
        Intent intent = new Intent(this, DisplayPhoto.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void renameAlbum(View view) {
        Bundle bundle = new Bundle();
        //bundle.putParcelable("AM", AM);
        Intent intent = new Intent(this, RenameAlbum.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}

class ImageAdapter extends ArrayAdapter<Photo> {

    private ArrayList<Photo> items;
    Context context;

    public ImageAdapter(Activity context, int viewResourceId, ArrayList<Photo> photos) {
        super(context, viewResourceId, photos);
        items = photos;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.photo_list_item, parent, false);
        }

        Photo it = items.get(position);
        String picturePath = it.getPath();
        ImageView imageView = (ImageView) v.findViewById(R.id.IvPicture);
        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        TextView textv = (TextView) v.findViewById(R.id.tvTitle);
        textv.setText(it.getPath());

//        File file = new File(it.getPath());
//        Bitmap bit = BitmapFactory.decodeFile(file.getAbsolutePath());
//        ImageView picture = v.findViewById(R.id.IvPicture);
//        picture.setImageBitmap(bit);

        return v;
    }
}