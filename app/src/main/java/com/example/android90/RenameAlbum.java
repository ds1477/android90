package com.example.android90;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;

import java.io.IOException;

public class RenameAlbum extends AppCompatActivity{

    public static final String ALBUM_INDEX = "albumIndex";
    public static final String ALBUM_NAME = "albumName";

    private int albumIndex;
    private TextView albumName;
    private EditText newAlbumName;
    private TextView oldName;
    AlbumManager AM;
    Album activeAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_rename);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        newAlbumName = findViewById(R.id.newAlbumName);
        oldName = findViewById(R.id.oldTitle);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //AM = (AlbumManager) bundle.getParcelable("AM");
            //activeAlbum = AM.getActiveAlbum();
            //oldName.setText("Current Name: " + activeAlbum.name);
        }

        try {
            AM = AlbumManager.deserialize();
            activeAlbum = AM.getActiveAlbum();
            oldName.setText("Current Name: " + activeAlbum.name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void goBack(View view){
        //setResult(RESULT_CANCELED);
        finish();
    }

    public void confirm(View view) {
        // gather all data from text fields
        String name = newAlbumName.getText().toString();

        if (name.trim().length() == 0) {
            Bundle bundle = new Bundle();
            bundle.putString(AlbumDialogFragment.MESSAGE_KEY,
                    "Please enter a valid album name.");
            DialogFragment newFragment = new AlbumDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }

        // pop up dialog if errors in input, and return
        // name and year are mandatory
        if (name == null || name.length() == 0) {
            Bundle bundle = new Bundle();
            bundle.putString(AlbumDialogFragment.MESSAGE_KEY,
                    "New album name is required.");
            DialogFragment newFragment = new AlbumDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }

        if(AM.changeAlbum(activeAlbum.name, name)) {
            // make Bundle
            Bundle bundle = new Bundle();
            //bundle.putParcelable("AM", AM);
            bundle.putInt(ALBUM_INDEX, albumIndex);
            bundle.putString(ALBUM_NAME,name);
            try {
                AlbumManager.serialize(AM);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // send back to caller
            Intent intent = new Intent(this, InsideAlbum.class);
            intent.putExtras(bundle);
            //setResult(RESULT_OK,intent);
            //finish(); // pops activity from the call stack, returns to parent
            startActivity(intent, bundle);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(AlbumDialogFragment.MESSAGE_KEY,
                    "This album already exists.");
            DialogFragment newFragment = new AlbumDialogFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return; // does not quit activity, just returns from method
        }


    }

}

