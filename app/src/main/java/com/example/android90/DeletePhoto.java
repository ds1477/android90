package com.example.android90;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.io.IOException;
import java.util.ArrayList;

public class DeletePhoto extends AppCompatActivity {

    private ListView listview;
    AlbumManager AM;
    Album activeAlbum;
    private Context c = this;
    ArrayList<Photo> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gen_del_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            AM = AlbumManager.deserialize();
            activeAlbum = AM.getActiveAlbum();
            photos = activeAlbum.getPhotos();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (AM == null) {
            AM = new AlbumManager();
            AM.addAlbum("Test");
        }


        listview = findViewById(R.id.del_list);
        listview.setAdapter(new ImageAdapter(this, R.layout.general_delete, photos));
        listview.setOnItemClickListener((p, V, pos, id) -> {
            try {
                delete(pos);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void delete(int pos) throws IOException {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Confirmation");
        builder.setMessage("Are you sure you want to delete this photo?");
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activeAlbum.deletePhoto(photos.get(pos));
                        try {
                            AlbumManager.serialize(AM);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(c, InsideAlbum.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
