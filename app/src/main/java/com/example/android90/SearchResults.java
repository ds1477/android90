package com.example.android90;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.util.ArrayList;

public class SearchResults extends AppCompatActivity {
    private ListView listview;
    AlbumManager AM;
    ArrayList<Photo> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            AM = AlbumManager.deserialize();
            Album results = AM.getSearchResults();
            photos = results.getPhotos();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        listview = findViewById(R.id.res_list);
        listview.setAdapter(new ImageAdapter(this, R.layout.search_results, photos));
    }

}
