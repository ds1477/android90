package com.example.android90.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The Album class stores information for an Album, and has methods for editing and retrieving information about the photos inside the album.
 */

public class Album implements Serializable {

    /**
     * Serial Version ID is set to 1
     */
    public static final long serialVersionUID = 1L;

    /**
     * Name of the album. Uniquely identifies an album.
     */
    public String name;

    /**
     * Stores all the photos in a particular album.
     */
    protected ArrayList<Photo> photos;

    protected Photo activePhoto;

    /**
     * Album constructor, accepts one String argument.
     *
     * @param name
     */
    public Album(String name) {
        this.name = name;
        photos = new ArrayList<Photo>();
        activePhoto = null;
    }

    protected Album(Parcel in) {
        name = in.readString();
        photos = new ArrayList<Photo>();
        activePhoto = in.readParcelable(Photo.class.getClassLoader());
    }

//    public static final Creator<Album> CREATOR = new Creator<Album>() {
//        @Override
//        public Album createFromParcel(Parcel in) {
//            return new Album(in);
//        }
//
//        @Override
//        public Album[] newArray(int size) {
//            return new Album[size];
//        }
//    };

    public String toString() {
        return this.name;
    }

    public boolean hasPhoto(String path) {
        for (Photo photo: photos) {
            if (photo.imagePath.equals(path)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes a photo from the album. Returns true if success, false if photo is not in album.
     *
     * @param photo
     */
    public boolean deletePhoto(Photo photo) {
        if (photos.contains(photo)) {
            int index = photos.indexOf(photo);
            photos.remove(index);
            return true;
        }
        return false;
    }

    /**
     * Adds a photo to the album directly. Particularly used for adding stock photos to the stock account's Stock album.
     * Only use when photo is safely add-able.
     *
     * @param photo
     */
    public void addStockPhoto(Photo photo) {
        photos.add(photo);
    }

    /**
     * Returns a photo object given a particular path String.
     *
     * @param path
     * @return photo
     */
    public Photo getPhoto(String path) {
        for (Photo photo : photos) {
            if (photo.imagePath.equals(path)) {
                return photo;
            }
        }
        return null;
    }

    /**
     * Returns all photos in the album
     *
     * @return ArrayList of photos
     */
    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    /**
     * Returns the activePhoto
     *
     * @return activePhoto
     */
    public Photo getActivePhoto() {
        return this.activePhoto;
    }

    /**
     * Returns the name of an album.
     *
     * @param album
     * @return album name
     */
    public static String getAlbumName(Album album) {
        return album.name;
    }

    /**
     * Returns the number of photos in the album.
     *
     * @return int
     */
    public int getNumberOfPhotos() {
        return photos.size();
    }

    public int getPhotoIndex(String path) {
        int i = -1;
        for (Photo photo : photos) {
            i += 1;
            if (photo.getPath().equals(path)) {
                return i;
            }
        }
        return -1; //although this shouldn't happen
    }

    public void removePhoto(String path) {
                int index = getPhotoIndex(path);
                photos.remove(index);

        return;
    }

    //Required methods for parcelable, I don't really know what these do
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(name);
//        dest.writeTypedList(photos);
//        dest.writeParcelable(activePhoto, flags);
//    }
}
