package com.example.android90.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The Photo class stores information for a User, and has methods for editing and retrieving information about individual photos.
 */

public class Photo implements Serializable {

    /**
     * Serial Version ID is set to 1
     */
    public static final long serialVersionUID = 1L;

    /**
     * Path of the image as a string. Uniquely identifies a Photo
     */
    protected String imagePath;

    /**
     * Caption of the photo
     */
    protected String caption;

    /**
     * List of tags associated with the photo
     */
    protected ArrayList<String> tags;

    /**
     * The system date associated with the image
     */
    protected Date date;

    /**
     * One argument Photo constructor. Accepts the path of the image as a string.
     *
     * @param path
     */
    public Photo(String path) {
        imagePath = path;
        caption = "";
        tags = new ArrayList<String>();
    }

    protected Photo(Parcel in) {
        imagePath = in.readString();
        caption = in.readString();
        tags = in.createStringArrayList();
    }

//    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
//        @Override
//        public Photo createFromParcel(Parcel in) {
//            return new Photo(in);
//        }
//
//        @Override
//        public Photo[] newArray(int size) {
//            return new Photo[size];
//        }
//    };

    /**
     * Returns the tags of a photo as an ArrayList of strings
     *
     * @return tags
     */
    public ArrayList<String> getTags() {
        return tags;
    }

    /**
     * Sets the caption of the photo
     *
     * @param text
     */
    public void setCaption(String text) {
        caption = text;
    }

    /**
     * Returns the caption of a photo
     *
     * @return caption
     */
    public String getCaption() {
        return this.caption;
    }

    /**
     * Returns the image path of the photo as a String
     *
     * @return path
     */
    public String getPath() {
        return this.imagePath;
    }

    /**
     * Adds a tap to the Photo. Returns true if success, false if tag has already been added.
     *
     * @param tag
     * @param value
     * @return boolean
     */
    public boolean addTag(String tag, String value) {
        String toAdd = "[" + tag + ": " + value + "]";
        if (tags.contains(toAdd)) {
            return false;
        } else {
            tags.add(toAdd);
            return true;
        }

    }

    /**
     * Deletes a tag from the photo. Accepts a String of the form [Tag: Value].
     *
     * @param tagValue
     */
    public void deleteTag(String tagValue) {
        int index = tags.indexOf(tagValue);
        tags.remove(index);
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(imagePath);
//        dest.writeString(caption);
//        dest.writeStringList(tags);
//    }
}