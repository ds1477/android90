package com.example.android90.model;

import android.content.Context;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.android90.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.channels.NonWritableChannelException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Daniel Samojlik and Elaine Yang
 * <p>
 * The Terminal class stores information about all users, and is used to keep track of what the system is doing.
 */

public class AlbumManager implements Serializable {

    /**
     * The location of the dat file is in the src folder
     */
    public static final String storeDir = "src";

    /**
     * Data is stored in userInfo.dat
     */
    public static final String storeFile = "userInfo.dat";

    /**
     * Serial Version ID is set to 1
     */
    public static final long serialVersionUID = 1L;

    /**
     * Stores all the albums that the User has created
     */
    protected ArrayList<Album> albums;

    /**
     * Stores all the tag types, including preloaded tag types and user created
     */
    //protected ArrayList<String> tagTypes;


    /**
     * Constructor for a User. Accepts one String argument.
     */
    public AlbumManager() {
        albums = new ArrayList<Album>();
        activeAlbum = null;
        activePhoto = null;
        searchResults = null;
    }

    /**
     * The album that has been opened, if applicable (is null otherwise)
     */
    protected Album activeAlbum;

    /**
     * The photo that is being acted upon, if applicable (is null otherwise)
     */
    protected Photo activePhoto;

    protected Album searchResults;


//    protected AlbumManager(Parcel in) {
//        albums = in.createTypedArrayList(Album.CREATOR);
//        activeAlbum = in.readParcelable(Album.class.getClassLoader());
//        activePhoto = in.readParcelable(Photo.class.getClassLoader());
//    }

//    public static final Creator<AlbumManager> CREATOR = new Creator<AlbumManager>() {
//        @Override
//        public AlbumManager createFromParcel(Parcel in) {
//            return new AlbumManager(in);
//        }
//
//        @Override
//        public AlbumManager[] newArray(int size) {
//            return new AlbumManager[size];
//        }
//    };

    /**
     * Deserializes the data from storage. Returns a terminal.
     *
     * @return terminal
     */
    @SuppressWarnings("deprecation")
    public static AlbumManager deserialize() throws ClassNotFoundException {
        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            File file = new File(dir, "info.bin");
            file.createNewFile();
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            AlbumManager AM = (AlbumManager) ois.readObject();
            ois.close();
            fis.close();
            return AM;
        } catch (IOException e) {
            return null;
        }
    } //This is for deserializing objects from storage

    /**
     * Serializes a terminal, called with closing the application.
     *
     * @param term
     */
    @SuppressWarnings("deprecation")
    public static void serialize(AlbumManager term) throws IOException {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(dir, "info.bin");
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file, false);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(term);
        oos.close();
        fos.close();
    } //This is for serializing objects (putting into storage)

    /**
     * Sets the activePhoto given a path String.
     *
     * @param path
     */
    public void setActivePhoto(String path) {
        for (Photo photo : activeAlbum.getPhotos()) {
            if (photo.imagePath.equals(path)) {
                this.activePhoto = photo;
            }
        }
    }

    public void setActiveAlbum(String name) {
        for (Album album: albums) {
            if (album.name.equals(name)) {
                this.activeAlbum = album;
                return;
            }
        }
        return;
    }

    public void setSearchResults(Album album) {
        searchResults = album;
    }

    public Album getSearchResults() {
        return searchResults;
    }

    /**
     * Sets the activeAlbum to null
     */
    public void closeActiveAlbum() {
        this.activeAlbum = null;
    }

    /**
     * Sets the activePhoto to null
     */
    public void closeActivePhoto() {
        this.activePhoto = null;
    }

    /**
     * Returns the activeAlbum
     *
     * @return activeAlbum
     */
    public Album getActiveAlbum() {
        return this.activeAlbum;
    }

    /**
     * Returns the activePhoto
     *
     * @return activePhoto
     */
    public Photo getActivePhoto() {
        return this.activePhoto;
    }

    /**
     * Used for returning the tag types of a user.
     *
     * @return ArrayList of strings, tagTypes
     */
    //public ArrayList<String> getTagTypes() {
//        return tagTypes;
//    }

    /**
     * Adds a tag to the list of tag types. Returns true if success, false if tag type already is in list.
     *
     * @param tag
     * @return boolean
     */
//    public boolean addTagType(String tag) {
//        if (tagTypes.contains(tag)) {
//            return false;
//        } else {
//            tagTypes.add(tag);
//            return true;
//        }
//    }

    /**
     * Used to see if a user has already created an album. Returns true if the album exists, false if not.
     *
     * @param name
     * @return boolean
     */
    public boolean hasAlbum(String name) {
        for (String albumname : this.getAlbumNames()) {
            if (albumname.equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds an album to a user's list of albums. Returns true if success, false if album already exists.
     *
     * @param name
     * @return boolean
     */
    public boolean addAlbum(String name) {
        if (albums.isEmpty()) {
            Album newAlbum = new Album(name);
            albums.add(newAlbum);
            return true;
        } else if (!albums.isEmpty()) {
            ArrayList<String> names = this.getAlbumNames();
            if (!names.contains(name)) {
                Album newAlbum = new Album(name);
                albums.add(newAlbum);
                return true;
            }
        }
        return false;
    }

    /**
     * Used for updating the name of an album. Returns true if success, false if name to be changed is not an existent album.
     *
     * @param oldName
     * @param newName
     * @return boolean
     */
    public boolean changeAlbum(String oldName, String newName) {
        for (Album a: albums) {
            String albumName = Album.getAlbumName(a);
            if (albumName.equals(oldName) && !albums.contains(getAlbum(newName))) {
                a.name = newName;
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes an album. Returns true if success, false if album not found.
     *
     * @param name
     * @return boolean
     */
    public boolean deleteAlbum(String name) {
        Album a = null;
        for (int i = 0; i < albums.size(); i++) {
            Album album = albums.get(i);
            String albumName = Album.getAlbumName(album);
            if (albumName.equals(name)) {
                a = album;
                int index = albums.indexOf(a);
                albums.remove(index);
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a photo to an album. Returns true if success, false if image is already in album.
     *
     * @param photo
     * @param album
     * @return boolean
     */
    public boolean addPhoto(Photo photo, Album album) {
        for (Photo p: album.photos) {
            if (p.imagePath.equals(photo.imagePath)) {
                return false;
            }
        }
        ArrayList<Photo> allPhotos = this.getAllPhotos();
        Photo toAdd = photo;
        for (Photo p : allPhotos) {
            if (p.imagePath.equals(photo.imagePath)) {
                toAdd = p;
            }
        }
        album.photos.add(toAdd);
        return true;
    }

    /**
     * Gets an album, given an album name.
     *
     * @param name
     * @return album
     */
    public Album getAlbum(String name) {
        for (int i = 0; i < albums.size(); i++) {
            Album album = albums.get(i);
            String albumName = Album.getAlbumName(album);
            if (albumName.equals(name)) {
                return album;
            }
        }
        return null;
    }

    /**
     * Returns the list of all albums for a user.
     *
     * @return ArrayList of albums
     */
    public ArrayList<Album> getAllAlbums() {
        return albums;
    }

    /**
     * Returns the list of all photos in all albums for a user.
     *
     * @return ArrayList of photos
     */
    public ArrayList<Photo> getAllPhotos() {
        ArrayList<Photo> allphotos = new ArrayList<Photo>();
        for (Album album : albums) {
            for (Photo photo : album.getPhotos()) {
                if (!allphotos.contains(photo)) {
                    allphotos.add(photo);
                }
            }
        }
        return allphotos;
    }

    /**
     * Returns a list of all album names for a user.
     *
     * @return ArrayList of Strings
     */
    public ArrayList<String> getAlbumNames() {
        ArrayList<String> albumnames = new ArrayList<String>();
        for (Album album : albums) {
            albumnames.add(album.name);
        }
        return albumnames;
    }

    //Required methods for parcelable
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeTypedList(albums);
//        //dest.writeStringList(tagTypes);
//        dest.writeParcelable(activeAlbum, flags);
//        dest.writeParcelable(activePhoto, flags);
//    }
}