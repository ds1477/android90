package com.example.android90;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android90.model.AlbumManager;

import java.io.IOException;
import java.util.ArrayList;

import com.example.android90.model.Album;
import com.example.android90.model.Photo;

public class Albums extends AppCompatActivity {
    private ListView listview;
    private ArrayList<Album> albums;
    private ArrayAdapter<Album> arrayAdapter;
    AlbumManager AM;
    private Context c = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums_list);

        //Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            AM = (AlbumManager) bundle.getParcelable("AM");
//        } else {
//            AM = new AlbumManager();
//            AM.addAlbum("Test");
//            Album test = AM.getAlbum("Test");
//            Photo test2 = new Photo("/storage/emulated/0/Pictures/IMG_20211212_174954.jpg");
//        }

        try {
            AM = AlbumManager.deserialize();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (AM == null) {
            AM = new AlbumManager();
            AM.addAlbum("Test");
        }

//        AM = new AlbumManager();
//        AM.addAlbum("Test");
//
//        try {
//            AlbumManager.serialize(AM);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        albums = AM.getAllAlbums();

        listview = findViewById(R.id.albums_list);
        arrayAdapter = new ArrayAdapter<Album>(this, R.layout.albums, albums);
        listview.setAdapter(arrayAdapter);
        listview.setOnItemClickListener((p, V, pos, id) -> {
            try {
                openAlbum(pos);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(view -> {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
            View mView = layoutInflaterAndroid.inflate(R.layout.activity_album_dialogbox, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
            alertDialogBuilderUserInput.setView(mView);
            final EditText albumNamefromDialog = (EditText) mView.findViewById(R.id.userInputDialog);

            alertDialogBuilderUserInput
                    .setCancelable(true)
                    .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {

                            String albumName = albumNamefromDialog.getText().toString();

                            if (albumName.trim().length() == 0) {
                                Toast.makeText(getApplicationContext(), "Please enter a valid album name", Toast.LENGTH_LONG).show();
                                return; //duplicate album name not allowed
                            }

                            if (AM.addAlbum(albumName)) {
                                try {
                                    AlbumManager.serialize(AM);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                listview = (ListView) findViewById(R.id.albums_list);
                                arrayAdapter.notifyDataSetChanged();
                                listview.setAdapter(arrayAdapter);
                                try {
                                    AlbumManager.serialize(AM);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                //refreshing ends here
                            } else {
                                Toast.makeText(getApplicationContext(), "Duplicate Album Names Not Allowed! Try Another Name", Toast.LENGTH_LONG).show();
                                return; //duplicate album name not allowed
                            }
                        }
                    })

                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    dialogBox.cancel();
                                }
                            });

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();
        });

        Button deleteBtn = (Button) findViewById(R.id.deleteBtn);
        deleteBtn.setOnClickListener(view -> {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
            View mView = layoutInflaterAndroid.inflate(R.layout.del_album, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
            alertDialogBuilderUserInput.setView(mView);
            final EditText albumNamefromDialog = (EditText) mView.findViewById(R.id.userInputDialog);

            alertDialogBuilderUserInput
                    .setCancelable(true)
                    .setPositiveButton("Delete Album", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {

                            String albumName = albumNamefromDialog.getText().toString();

                            if (AM.deleteAlbum(albumName)) {
                                try {
                                    AlbumManager.serialize(AM);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                listview = (ListView) findViewById(R.id.albums_list);
                                arrayAdapter.notifyDataSetChanged();
                                listview.setAdapter(arrayAdapter);
                                //refreshing ends here
                            } else {
                                Toast.makeText(getApplicationContext(), "Album does not exist! Try Another Name", Toast.LENGTH_LONG).show();
                                return; //album name cannot be found
                            }
                        }
                    })

                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    dialogBox.cancel();
                                }
                            });

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();
        });
    }

    public void openAlbum(int pos) throws IOException {
        Bundle bundle = new Bundle();
        Album album = albums.get(pos);
        AM.setActiveAlbum(Album.getAlbumName(album));
        AlbumManager.serialize(AM);
        //bundle.putParcelable("AM", AM);
        bundle.putInt(InsideAlbum.ALBUM_INDEX, pos);
        bundle.putString(InsideAlbum.ALBUM_NAME, album.name);
        Intent intent = new Intent(this, InsideAlbum.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void searchPhotos(View view) { //todo
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, SearchPhotos.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}
