package com.example.android90;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.example.android90.AlbumDialogFragment;
import com.example.android90.InsideAlbum;
import com.example.android90.R;
import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.io.IOException;

public class AddTag extends AppCompatActivity {
    public EditText tagfromDialog;
    public RadioButton person;
    public RadioButton location;

    AlbumManager AM;
    Album activeAlbum;
    Photo activePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_tag);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tagfromDialog = (EditText) findViewById(R.id.newTag);
        person = findViewById(R.id.person);
        location = findViewById(R.id.location);


        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //AM = (AlbumManager) bundle.getParcelable("AM");
            //activeAlbum = AM.getActiveAlbum();
            //oldName.setText("Current Name: " + activeAlbum.name);
        }

        try {
            AM = AlbumManager.deserialize();
            activeAlbum = AM.getActiveAlbum();
            //albumName.setText(activeAlbum.name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void goBack(View view){
        //setResult(RESULT_CANCELED);
        finish();
    }

    public void createItem(View view) {

        if (person.isChecked() || location.isChecked()) {
            if (TextUtils.isEmpty(tagfromDialog.getText())) {
                Toast.makeText(getApplicationContext(), "Error. Please enter a tag value.", Toast.LENGTH_LONG).show();
                return;
            }

            String tag = tagfromDialog.getText().toString();
            if (tag.trim().length() == 0) {
                Toast.makeText(getApplicationContext(), "Error. Please enter a valid tag value.", Toast.LENGTH_LONG).show();
                return;
            }

            String tagValue;
            if (person.isChecked()) {
                tagValue = "person";
            } else {
                tagValue = "location";
            }
            if (AM.getActivePhoto().addTag(tagValue, tag)) {
                try {
                    AlbumManager.serialize(AM);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Bundle bundle = new Bundle();
                Intent intent = new Intent(this, DisplayPhoto.class);
                intent.putExtras(bundle);
                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), "This tag already exists. Please use another tag.", Toast.LENGTH_SHORT).show();
                return;
            }

//            listview = (ListView) findViewById(R.id.tags);
//            arrayAdapter.notifyDataSetChanged();
//            listview.setAdapter(arrayAdapter);

        } else {
            Toast.makeText(getApplicationContext(), "Error. Please select a tag type.", Toast.LENGTH_LONG).show();
            return;
        }
    }

}
