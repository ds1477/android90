package com.example.android90;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.android90.model.Album;
import com.example.android90.model.AlbumManager;
import com.example.android90.model.Photo;

import java.io.IOException;
import java.util.ArrayList;

public class DisplayPhoto extends AppCompatActivity {
    private ArrayList<String> tags;
    private ListView listview;
    private ArrayAdapter<String> arrayAdapter;

    AlbumManager AM;
    Photo activePhoto;
    Album activeAlbum;
    Context c = this;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_content);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // activates the up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            pos = bundle.getInt("index");
//        }

        try {
            AM = AlbumManager.deserialize();
            activePhoto = AM.getActivePhoto();
            activeAlbum = AM.getActiveAlbum();
            pos = activeAlbum.getPhotoIndex(activePhoto.getPath());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        tags = AM.getActivePhoto().getTags();
        listview = findViewById(R.id.tags);
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.display, tags);
        listview.setAdapter(arrayAdapter);

        ImageView imageView = findViewById(R.id.imageView);
        String picturePath = activePhoto.getPath();
        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));


        Button moveBtn = (Button) findViewById(R.id.move);
        moveBtn.setOnClickListener(view -> {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
            View mView = layoutInflaterAndroid.inflate(R.layout.move_dialog, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
            alertDialogBuilderUserInput.setView(mView);
            final EditText albumNamefromDialog = (EditText) mView.findViewById(R.id.userInputDialog);

            alertDialogBuilderUserInput
                    .setCancelable(true)
                    .setPositiveButton("Move", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogBox, int id) {

                            String albumName = albumNamefromDialog.getText().toString();

                            if (albumName.equals(activeAlbum.name)) {
                                Toast.makeText(getApplicationContext(), "Photo is already in this album!", Toast.LENGTH_LONG).show();
                                return;
                            } else if (!AM.hasAlbum(albumName)) {
                                Toast.makeText(getApplicationContext(), "Album does not exist! Try Another Name", Toast.LENGTH_LONG).show();
                                return; //album name cannot be found
                            } else {
                                Album maybe = AM.getAlbum(albumName);
                                if (maybe.hasPhoto(activePhoto.getPath())) {
                                    Toast.makeText(getApplicationContext(), "Photo is already in this album!", Toast.LENGTH_LONG).show();
                                    return; //album name cannot be found
                                }
                                maybe.addStockPhoto(activePhoto);
                                activeAlbum.removePhoto(activePhoto.getPath());
                                try {
                                    AlbumManager.serialize(AM);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                back();
                            }
                        }
                    })

                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogBox, int id) {
                                    dialogBox.cancel();
                                }
                            });

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();
        });

    }

    public void back() {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, InsideAlbum.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void addTag(View view) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, AddTag.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Bundle bundle = new Bundle();
        //bundle.putParcelable("AM", AM);
        Intent intent = new Intent(this, InsideAlbum.class);
        intent.putExtras(bundle);
        startActivity(intent, bundle);
        return true;
    }

    public void deleteTag(View view) {
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, DeleteTag.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void toNext(View view) {
        int ref = activeAlbum.getNumberOfPhotos() - 1;
        if (pos == ref) {
            Toast.makeText(getApplicationContext(), "This is the last photo in the album!", Toast.LENGTH_LONG).show();
        } else {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(this, DisplayPhoto.class);
            intent.putExtras(bundle);
            ArrayList<Photo> photos = activeAlbum.getPhotos();
            Photo photo = photos.get(pos+1);
            AM.setActivePhoto(photo.getPath());
            try {
                AlbumManager.serialize(AM);
            } catch (IOException e) {
                e.printStackTrace();
            }
            startActivity(intent);
        }
    }

    public void toPrev(View view) {
        //int ref = activeAlbum.getNumberOfPhotos() - 1;
        if (pos == 0) {
            Toast.makeText(getApplicationContext(), "This is the first photo in the album!", Toast.LENGTH_LONG).show();
        } else {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(this, DisplayPhoto.class);
            intent.putExtras(bundle);
            ArrayList<Photo> photos = activeAlbum.getPhotos();
            Photo photo = photos.get(pos-1);
            AM.setActivePhoto(photo.getPath());
            try {
                AlbumManager.serialize(AM);
            } catch (IOException e) {
                e.printStackTrace();
            }
            startActivity(intent);
        }
    }


}
